# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Priit Jõerüüt <transifex@joeruut.com>, 2021-2022
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-08 12:56+0200\n"
"PO-Revision-Date: 2013-07-03 19:27+0000\n"
"Last-Translator: Priit Jõerüüt <transifex@joeruut.com>, 2021-2022\n"
"Language-Team: Estonian (http://www.transifex.com/xfce/xfce-panel-plugins/language/et/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: et\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/wavelan.c:274 ../panel-plugin/wi_common.c:35
msgid "No carrier signal"
msgstr "Teenuse signaal puudub"

#. Translators: net_name: quality quality_unit at rate Mb/s
#: ../panel-plugin/wavelan.c:296
#, c-format
msgid "%s: %d%s at %dMb/s"
msgstr "%s: %d%s kiirusega %dMb/s"

#. Translators: quality quality_unit at rate Mb/s
#: ../panel-plugin/wavelan.c:299
#, c-format
msgid "%d%s at %dMb/s"
msgstr "%d%s kiirusega %dMb/s"

#: ../panel-plugin/wavelan.c:303
msgid "No device configured"
msgstr "Seade on kirjeldamata"

#: ../panel-plugin/wavelan.c:434
#, c-format
msgid ""
"<big><b>Failed to execute command \"%s\".</b></big>\n"
"\n"
"%s"
msgstr "<big><b>„%s“ käsu käivitamine ei õnnestunud.</b></big>\n\n%s"

#: ../panel-plugin/wavelan.c:437
msgid "Error"
msgstr "Viga"

#: ../panel-plugin/wavelan.c:691
msgid "Wavelan Plugin Options"
msgstr "Traadita võrguseadme jälgija eelistused"

#: ../panel-plugin/wavelan.c:704
msgid "Properties"
msgstr "Omadused"

#: ../panel-plugin/wavelan.c:715
msgid "Interface"
msgstr "Võrguliides"

#: ../panel-plugin/wavelan.c:738
msgid "_Autohide when offline"
msgstr "Peida automaatselt, kui _võrguühendus puudub"

#: ../panel-plugin/wavelan.c:748
msgid "Autohide when no _hardware present"
msgstr "Peida automaatselt, kui arvutis ei leidu vastavat _raudvara"

#: ../panel-plugin/wavelan.c:760
msgid ""
"Note: This will make it difficult to remove or configure the plugin if there"
" is no device detected."
msgstr "Märkus: kui raudvara puudumisel peidad selle vidina automaatselt, siis on sul raske seda hiljem seadistada ja eemaldada."

#: ../panel-plugin/wavelan.c:769
msgid "Show _icon"
msgstr "Näita _ikooni"

#: ../panel-plugin/wavelan.c:780
msgid "Show signal _bar"
msgstr "Näita signaalitugevuse _tulpa"

#: ../panel-plugin/wavelan.c:791
msgid "Enable sig_nal quality colors"
msgstr "Märgista värvidega _signaali kvaliteeti"

#: ../panel-plugin/wavelan.c:802
msgid "Wifi Manager Command"
msgstr "Wifihalduri käsk"

#: ../panel-plugin/wavelan.c:834 ../panel-plugin/wavelan.desktop.in.h:2
msgid "View the status of a wireless network"
msgstr "Vaata traadita võrgu olekut"

#: ../panel-plugin/wi_common.c:38
msgid "No such WaveLAN device"
msgstr "Sellist traadita võrguseadet ei leidu"

#: ../panel-plugin/wi_common.c:41
msgid "Invalid parameter"
msgstr "Vigane parameeter"

#: ../panel-plugin/wi_common.c:44
msgid "Unknown error"
msgstr "Teadmata viga"

#: ../panel-plugin/wi_linux.c:143
msgid "Unknown"
msgstr "Pole teada"

#: ../panel-plugin/wavelan.desktop.in.h:1
msgid "Wavelan"
msgstr "Traadita võrguseade"
